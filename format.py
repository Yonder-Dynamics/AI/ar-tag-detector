import os
import numpy as np
import pandas as pd
import sys
import cv2
import copy

class_name = "tag"
class_id = 0

# Create dataframes for csvs
annot = pd.read_csv(sys.argv[1])

# Dropping unneeded columns from the bounding box files
cw = os.getcwd()
annot["filename"] = [cw + "/" + fn for fn in annot["filename"]]
names = annot["filename"].tolist()

# names.to_csv("lists.txt", index=False)

annot = annot.drop(["filename", "class"], axis=1)

# Calculate x, y, width, height relative to image height

x = [coord / dim for coord, dim in zip(annot["xmin"], annot["width"])]
y = [coord / dim for coord, dim in zip(annot["ymin"], annot["height"])]

width = [(xmax - xmin) / dim for xmax, xmin, dim in zip(annot["xmax"], annot["xmin"], annot["width"])]
height = [(ymax - ymin) / dim for ymax, ymin, dim in zip(annot["ymax"], annot["ymin"], annot["height"])]
classes = np.full((annot.shape[0]), 0)

annot = annot.drop(["width", "height", "xmin", "ymin", "xmax", "ymax"], axis=1)

annot["object-class"] = classes
annot["x"] = x
annot["y"] = y
annot["width"] = width
annot["height"] = height


for filename, entry in zip(names, annot.itertuples()):
    data = [entry[1:]]
    df = pd.DataFrame.from_records(data, columns=["object-class", "x", "y",
        "width", "height"])
    df.to_csv(filename[:len(filename) - 4] + ".txt", index=False, header=False,
            sep=" ")
    # Augmenting
    img = cv2.imread(filename)
    # vFlip
    vertname = filename[:len(filename) - 4] + "vflip" + filename[len(filename) - 4:]
    vertflip = cv2.flip(img, 0)
    names.append(vertname)
    cv2.imwrite(vertname, vertflip)
    vflipdata = copy.deepcopy(data)
    vflipdata[0] = (vflipdata[0][0], vflipdata[0][1], 1 - vflipdata[0][2] -
                    vflipdata[0][4], vflipdata[0][3], vflipdata[0][4])
    df = pd.DataFrame.from_records(vflipdata, columns=["object-class", "x", "y",
        "width", "height"])
    df.to_csv(filename[:len(filename) - 4] + "vflip.txt", index=False, header=False,
            sep=" ")

    # hFlip
    horiname = filename[:len(filename) - 4] + "hflip" + filename[len(filename) - 4:]
    horiflip = cv2.flip(img, 1)
    names.append(horiname)
    cv2.imwrite(horiname, horiflip)
    hflipdata = copy.deepcopy(data)
    hflipdata[0] = (hflipdata[0][0], 1 - hflipdata[0][1] - hflipdata[0][3],
                    hflipdata[0][2], hflipdata[0][3], hflipdata[0][4])
    df = pd.DataFrame.from_records(hflipdata, columns=["object-class", "x", "y",
        "width", "height"])
    df.to_csv(filename[:len(filename) - 4] + "hflip.txt", index=False, header=False,
            sep=" ")

    # brigh
    for level in [-40.0, -20.0, 20.0, 40.0]:
        brightname = filename[:len(filename) - 4] + "bright" + str(level) + filename[len(filename) - 4:]
        bright = cv2.add(img, np.array([level]))
        names.append(brightname)
        cv2.imwrite(brightname, bright)
        df = pd.DataFrame.from_records(data, columns=["object-class", "x", "y",
            "width", "height"])
        df.to_csv(filename[:len(filename) - 4] + "bright" + str(level) + ".txt", index=False, header=False,
                sep=" ")

    # saturation
    for level in [.5, .75, 1.25, 1.5]:
        satname = filename[:len(filename) - 4] + "sat" + str(level) + filename[len(filename) - 4:]
        hsvImg = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
        hsvImg[...,1] = hsvImg[...,1]*level
        satimg=cv2.cvtColor(hsvImg,cv2.COLOR_HSV2BGR)
        names.append(satname)
        cv2.imwrite(satname, satimg)
        df = pd.DataFrame.from_records(data, columns=["object-class", "x", "y",
            "width", "height"])
        df.to_csv(filename[:len(filename) - 4] + "sat" + str(level) + ".txt", index=False, header=False,
                sep=" ")

    # salt & pepper
    for level in [40.0, 80.0, 120.0]:
        noisename = filename[:len(filename) - 4] + "noise" + str(level) + filename[len(filename) - 4:]
        rand = cv2.randn(np.zeros_like(img), (0, 0, 0), (level, level, level))
        noise = cv2.add(img, rand)
        names.append(noisename)
        cv2.imwrite(noisename, noise)
        df = pd.DataFrame.from_records(data, columns=["object-class", "x", "y",
            "width", "height"])
        df.to_csv(filename[:len(filename) - 4] + "noise" + str(level) + ".txt", index=False, header=False,
                sep=" ")

    # blur
    for ksize in [5, 9, 13]:
        blurname = filename[:len(filename) - 4] + "blur" + str(ksize) + filename[len(filename) - 4:]
        blurred = cv2.GaussianBlur(img, (ksize, ksize), 0)
        names.append(blurname)
        cv2.imwrite(blurname, blurred)
        df = pd.DataFrame.from_records(data, columns=["object-class", "x", "y",
            "width", "height"])
        df.to_csv(filename[:len(filename) - 4] + "blur" + str(ksize) + ".txt", index=False, header=False,
                sep=" ")


df = pd.DataFrame(names, columns=["colummn"])
df.to_csv("lists.txt", index=False, header=False)

