/*
./detect -r -d=10
 g++ $(pkg-config --cflags --libs opencv4) -std=c++11 detect_markers.cpp -o detect
By downloading, copying, installing or using the software you agree to this
license. If you do not agree to this license, do not download, install,
copy or use the software.

                          License Agreement
               For Open Source Computer Vision Library
                       (3-clause BSD License)

Copyright (C) 2013, OpenCV Foundation, all rights reserved.
Third party copyrights are property of their respective owners.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  * Neither the names of the copyright holders nor the names of the contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

This software is provided by the copyright holders and contributors "as is" and
any express or implied warranties, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose are
disclaimed. In no event shall copyright holders or contributors be liable for
any direct, indirect, incidental, special, exemplary, or consequential damages
(including, but not limited to, procurement of substitute goods or services;
loss of use, data, or profits; or business interruption) however caused
and on any theory of liability, whether in contract, strict liability,
or tort (including negligence or otherwise) arising in any way out of
the use of this software, even if advised of the possibility of such damage.
*/


#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#define NUM_BINS 16

using namespace std;
using namespace cv;

namespace {
const char* about = "Basic marker detection";
const char* keys  =
        "{d        |       | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2,"
        "DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, "
        "DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12,"
        "DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16,"
        "DICT_APRILTAG_16h5=17, DICT_APRILTAG_25h9=18, DICT_APRILTAG_36h10=19, DICT_APRILTAG_36h11=20}"
        "{v        |       | Input from video file, if ommited, input comes from camera }"
        "{ci       | 0     | Camera id if input doesnt come from video (-v) }"
        "{c        |       | Camera intrinsic parameters. Needed for camera pose }"
        "{l        | 0.1   | Marker side lenght (in meters). Needed for correct scale in camera pose }"
        "{dp       |       | File of marker detector parameters }"
        "{r        |       | show rejected candidates too }"
        "{refine   |       | Corner refinement: CORNER_REFINE_NONE=0, CORNER_REFINE_SUBPIX=1,"
        "CORNER_REFINE_CONTOUR=2, CORNER_REFINE_APRILTAG=3}";
}

/**
 */
static bool readCameraParameters(string filename, Mat &camMatrix, Mat &distCoeffs) {
    FileStorage fs(filename, FileStorage::READ);
    if(!fs.isOpened())
        return false;
    fs["camera_matrix"] >> camMatrix;
    fs["distortion_coefficients"] >> distCoeffs;
    return true;
}

void drawDetectedMarkers(InputOutputArray _image, InputArrayOfArrays _corners,
                         InputArray _ids, Scalar borderColor, vector<vector<Point2f>> rejected) {

    int min_size = 200;
    for(int i = 0; i < rejected.size(); i++) {
      // For each bounding box
    }

    CV_Assert(_image.getMat().total() != 0 &&
              (_image.getMat().channels() == 1 || _image.getMat().channels() == 3));
    CV_Assert((_corners.total() == _ids.total()) || _ids.total() == 0);

    // calculate colors
    Scalar textColor, cornerColor;
    textColor = cornerColor = borderColor;
    swap(textColor.val[0], textColor.val[1]);     // text color just sawp G and R
    swap(cornerColor.val[1], cornerColor.val[2]); // corner color just sawp G and B

    int nMarkers = (int)_corners.total();
    for(int i = 0; i < nMarkers; i++) {
        Mat currentMarker = _corners.getMat(i);
        CV_Assert(currentMarker.total() == 4 && currentMarker.type() == CV_32FC2);

        int a = 0;
        int b = 0;
        //for(int j = 0; j < rejected[i].size(); j++) {
           ////Vec3b pix = image.at<Vec3b>(rejected[i][j].y, rejected[i][j].x);
           //a += rejected[i][j].y * rejected[i][(j+1)%4].x;
           //b += rejected[i][j].y * rejected[i][(j+3)%4].x;
        //}
        for(int j = 0; j < 4; j++) {
            a += currentMarker.ptr< Point2f >(0)[j].y * currentMarker.ptr< Point2f >(0)[(j+1)%4].x;
            b += currentMarker.ptr< Point2f >(0)[j].y * currentMarker.ptr< Point2f >(0)[(j+3)%4].x;
            //cout << currentMarker.ptr< Point2f >(0)[j] << endl;
        }

        double area = abs(a-b);
        if(area < min_size) {
          continue;
        }
        Mat imgmat = _image.getMat();
        Mat greyimg;
        Mat unblurredGrey;
        cvtColor(imgmat, unblurredGrey, COLOR_BGR2GRAY);
        //greyimg.convertTo(greyimg, -1, 2, 0);
        GaussianBlur(unblurredGrey, greyimg, Size(5, 5), 0, 0);

        Mat mask(imgmat.rows, imgmat.cols, CV_8UC3, Scalar(0, 0, 0));
        int avgx = 0;
        int avgy = 0;

        // draw marker sides
        for(int j = 0; j < 4; j++) {
            Point2f p0, p1;
            p0 = currentMarker.ptr< Point2f >(0)[j];
            p1 = currentMarker.ptr< Point2f >(0)[(j + 1) % 4];
            //line(_image, p0, p1, borderColor, 1);
            line(mask, p0, p1, Scalar(255, 255, 255), 1);
            //cout << mask.at<Vec3b>(p0.y, p0.x) << endl;
            avgx += p0.x;
            avgy += p0.y;
        }
        avgx /= 4;
        avgy /= 4;
        // Flood fill

        vector<pair<int, int>> toExplore;
        toExplore.push_back(make_pair(avgx, avgy));
        int hist[NUM_BINS] = {0};
        int colorscore = 0;

        while(!toExplore.empty()) {
            pair<int, int> p = toExplore[toExplore.size() - 1];
            //cout << toExplore.size() << mask.at<Vec3f>(p.second, p.first) << p.first << ", " << p.second<<  endl;
            toExplore.pop_back();

            if(p.second < 0 || p.second > mask.rows - 1 || p.first < 0 || p.first > mask.cols || mask.at<Vec3b>(p.second, p.first)[0] == 255) {
                //cout << "skip" << endl;
                continue;
            }
            // set explored
            mask.at<Vec3b>(p.second, p.first)[0] = 255;

            hist[(int)greyimg.at<uchar>(p.second, p.first) / (256 / NUM_BINS)]++;

            colorscore += pow(abs(unblurredGrey.at<uchar>(p.second, p.first) - imgmat.at<Vec3b>(p.second, p.first)[0]), 2) / (double) area;
            colorscore += pow(abs(unblurredGrey.at<uchar>(p.second, p.first) - imgmat.at<Vec3b>(p.second, p.first)[1]), 2) / (double) area;
            colorscore += pow(abs(unblurredGrey.at<uchar>(p.second, p.first) - imgmat.at<Vec3b>(p.second, p.first)[2]), 2) / (double) area;
            //cout << (int)greyimg.at<uchar>(p.second, p.first) << endl;

            toExplore.push_back(make_pair(p.first + 1, p.second));
            toExplore.push_back(make_pair(p.first - 1, p.second));
            toExplore.push_back(make_pair(p.first, p.second + 1));
            toExplore.push_back(make_pair(p.first, p.second - 1));
        }
        //sort(hist, hist + num_bins);
        //cout << endl;
        // Calc bimodality
        // Smoothing
        int SMOOTH_NUM = 3;
        double SMOOTH_RATIO = .80;
        for(int i = 0; i < SMOOTH_NUM; i++) {
            for(int j = 0; j < NUM_BINS - 1; j++) {
                hist[j] = SMOOTH_RATIO * hist[j] +  (1 - SMOOTH_RATIO) * hist[j+1];
            }
            for(int j = 1; j < NUM_BINS; j++) {
                hist[j] = SMOOTH_RATIO * hist[j] +  (1 - SMOOTH_RATIO) * hist[j-1];
            }
        }


        int peaks = 0, c = 0;
        bool increasing = true;
        //int thresh = area / 300;
        int thresh = 0;
        vector<int> peaknums;
        for (int x=0;x<NUM_BINS - 1;x++) {
            if (hist[x]<hist[x+1]){ 
                if (!increasing &&(c-hist[x]>thresh)){ 
                    c=hist[x]; 
                }                         
                increasing = true;
            }
            else if (hist[x]>hist[x+1]){ 
                if (increasing &&(hist[x]-c>thresh)){ 
                    c=hist[x]; 
                    //peak[peaks]=x; 
                    peaknums.push_back(x);
                    peaks++; 
                } 
                increasing = false;
            } 
        }
        //if(hist[0] - thresh > hist[1]) peaks++;
        //if(hist[NUM_BINS - 1] - thresh > hist[NUM_BINS - 2]) peaks++;

        // draw first corner mark
        //rectangle(_image, currentMarker.ptr< Point2f >(0)[0] - Point2f(3, 3),
                  //currentMarker.ptr< Point2f >(0)[0] + Point2f(3, 3), cornerColor, 1, LINE_AA);

        // draw ID
        //if(_ids.total() != 0) {
        //cout << peaks << ": " << thresh << ": ";
        //for(int i = 0; i < NUM_BINS; i++) {
            //cout << hist[i] << "," ;
        //}
        //cout << endl;

        Point2f cent(0, 0);
        if(area > 1000) {
            for(int j = 0; j < 4; j++) {
                Point2f p0, p1;
                p0 = currentMarker.ptr< Point2f >(0)[j];
                p1 = currentMarker.ptr< Point2f >(0)[(j + 1) % 4];
                line(_image, p0, p1, (peaks == 2) ? Scalar(0, 255, 0) : Scalar(0, 0, 255), 1);
            }
            for(int p = 0; p < 4; p++) {
                cent += currentMarker.ptr< Point2f >(0)[p];
            }

            cent = cent / 4.;
            stringstream s;
            s << peaks << ": " << colorscore << " :";
            for(int i = 0; i < peaknums.size(); i++) {
                s << peaknums[i] << ", ";
            }
            //for(int i = 0; i < NUM_BINS; i++) {
                //s << hist[i] << "," ;
            //}
            putText(_image, s.str(), cent, FONT_HERSHEY_SIMPLEX, 0.5, textColor, 2);
        }
        //}
    }
}


/**
 */
static bool readDetectorParameters(string filename, Ptr<aruco::DetectorParameters> &params) {
    FileStorage fs(filename, FileStorage::READ);
    if(!fs.isOpened())
        return false;
    fs["adaptiveThreshWinSizeMin"] >> params->adaptiveThreshWinSizeMin;
    fs["adaptiveThreshWinSizeMax"] >> params->adaptiveThreshWinSizeMax;
    fs["adaptiveThreshWinSizeStep"] >> params->adaptiveThreshWinSizeStep;
    fs["adaptiveThreshConstant"] >> params->adaptiveThreshConstant;
    fs["minMarkerPerimeterRate"] >> params->minMarkerPerimeterRate;
    fs["maxMarkerPerimeterRate"] >> params->maxMarkerPerimeterRate;
    fs["polygonalApproxAccuracyRate"] >> params->polygonalApproxAccuracyRate;
    fs["minCornerDistanceRate"] >> params->minCornerDistanceRate;
    fs["minDistanceToBorder"] >> params->minDistanceToBorder;
    fs["minMarkerDistanceRate"] >> params->minMarkerDistanceRate;
    fs["cornerRefinementMethod"] >> params->cornerRefinementMethod;
    fs["cornerRefinementWinSize"] >> params->cornerRefinementWinSize;
    fs["cornerRefinementMaxIterations"] >> params->cornerRefinementMaxIterations;
    fs["cornerRefinementMinAccuracy"] >> params->cornerRefinementMinAccuracy;
    fs["markerBorderBits"] >> params->markerBorderBits;
    fs["perspectiveRemovePixelPerCell"] >> params->perspectiveRemovePixelPerCell;
    fs["perspectiveRemoveIgnoredMarginPerCell"] >> params->perspectiveRemoveIgnoredMarginPerCell;
    fs["maxErroneousBitsInBorderRate"] >> params->maxErroneousBitsInBorderRate;
    fs["minOtsuStdDev"] >> params->minOtsuStdDev;
    fs["errorCorrectionRate"] >> params->errorCorrectionRate;
    return true;
}



/**
 */
int main(int argc, char *argv[]) {
    CommandLineParser parser(argc, argv, keys);
    parser.about(about);

    if(argc < 2) {
        parser.printMessage();
        return 0;
    }

    int dictionaryId = parser.get<int>("d");
    bool showRejected = parser.has("r");
    bool estimatePose = parser.has("c");
    float markerLength = parser.get<float>("l");

    Ptr<aruco::DetectorParameters> detectorParams = aruco::DetectorParameters::create();
    if(parser.has("dp")) {
        bool readOk = readDetectorParameters(parser.get<string>("dp"), detectorParams);
        if(!readOk) {
            cerr << "Invalid detector parameters file" << endl;
            return 0;
        }
    }

    if (parser.has("refine")) {
        //override cornerRefinementMethod read from config file
        detectorParams->cornerRefinementMethod = parser.get<int>("refine");
    }
    std::cout << "Corner refinement method (0: None, 1: Subpixel, 2:contour, 3: AprilTag 2): " << detectorParams->cornerRefinementMethod << std::endl;

    int camId = parser.get<int>("ci");

    String video;
    if(parser.has("v")) {
        video = parser.get<String>("v");
    }

    if(!parser.check()) {
        parser.printErrors();
        return 0;
    }

    Ptr<aruco::Dictionary> dictionary =
        aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    Mat camMatrix, distCoeffs;
    if(estimatePose) {
        bool readOk = readCameraParameters(parser.get<string>("c"), camMatrix, distCoeffs);
        if(!readOk) {
            cerr << "Invalid camera file" << endl;
            return 0;
        }
    }

    VideoCapture inputVideo;
    int waitTime;
    if(!video.empty()) {
        inputVideo.open(video);
        waitTime = 0;
    } else {
        inputVideo.open(camId);
        waitTime = 10;
    }

    double totalTime = 0;
    int totalIterations = 0;

    while(inputVideo.grab()) {
        Mat image, imageCopy;
        inputVideo.retrieve(image);

        double tick = (double)getTickCount();

        vector< int > ids;
        vector< vector< Point2f > > corners, rejected;
        vector< Vec3d > rvecs, tvecs;

        // detect markers and estimate pose
        aruco::detectMarkers(image, dictionary, corners, ids, detectorParams, rejected);
        if(estimatePose && ids.size() > 0)
            aruco::estimatePoseSingleMarkers(corners, markerLength, camMatrix, distCoeffs, rvecs,
                                             tvecs);

        double currentTime = ((double)getTickCount() - tick) / getTickFrequency();
        totalTime += currentTime;
        totalIterations++;
        if(totalIterations % 30 == 0) {
            cout << "Detection Time = " << currentTime * 1000 << " ms "
                 << "(Mean = " << 1000 * totalTime / double(totalIterations) << " ms)" << endl;
        }

        // draw results
        image.copyTo(imageCopy);
        if(ids.size() > 0) {
            cout << ids.size() << endl;
            aruco::drawDetectedMarkers(imageCopy, corners, ids);

            if(estimatePose) {
                for(unsigned int i = 0; i < ids.size(); i++)
                    aruco::drawAxis(imageCopy, camMatrix, distCoeffs, rvecs[i], tvecs[i],
                                    markerLength * 0.5f);
            }
        }

        if(showRejected && rejected.size() > 0) {
            drawDetectedMarkers(imageCopy, rejected, noArray(), Scalar(100, 0, 255), rejected);

            // Use color histogram to determine viability of bounding box
        }
        

        imshow("out", imageCopy);
        char key = (char)waitKey(waitTime);
        if(key == 27) break;
    }

    return 0;
}

